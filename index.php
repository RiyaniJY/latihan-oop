<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
</head>
<body>
<?php

require('animal.php');
require('frog.php');
require('ape.php');

//release 0
$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo "<br>";
echo $sheep->legs; // 2
echo "<br>";
echo $sheep->cold_blooded ;// false
echo "<br><br>";

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
$kodok = new frog("buduk");
echo $kodok->name ." <br>";
$kodok->jump() ;
echo "<br><br>";
  $sungokong = new ape("Kera Sakti");
  echo $sungokong->name ." <br>";
  $sungokong->yell(); //"Auoo"




?>
</body>
</html>