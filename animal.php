<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Animal</title>
</head>
<body>
<?php
class Animal{

public $name;
public $legs = 2;
public $cold_blooded="false";

function __construct($nama){
    $this->name=$nama;
}

function getName(){
    return $this->name;
}

function getLeg(){
    return $this->legs;
}

function get_cold_blooded(){
    return $this->cold_blooded;
}

}




?>
</body>
</html>